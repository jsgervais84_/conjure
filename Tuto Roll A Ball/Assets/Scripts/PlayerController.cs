﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float Speed = 550.0f;
	
	// Update is called once per frame
	void FixedUpdate () {

		// Could use gyrscope data
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		//Moving in x-y plane only
		Vector3 movement = new Vector3(moveHorizontal,0.0f ,moveVertical);

		//object's rigid body -- player object.   Acceleration = speed * delta Time 
		rigidbody.AddForce (movement * Speed * Time.deltaTime);

	}
}
